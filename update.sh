#!/usr/bin/env bash
echo "Activating virtual environment..."
source venv/bin/activate
echo "Pulling from master..."
git pull origin master
echo "Installing requirements..."
pip install -r requirements.txt
echo "Making migrations..."
python manage.py makemigrations
echo "Migrating..."
python manage.py migrate
echo "Collecting static content..."
python3 manage.py collectstatic --noinput
echo "Restarting NginX and uWsgi"
systemctl restart nginx uwsgi